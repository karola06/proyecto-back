require('dotenv').config();
const express = require('express');
const app = express();  //inicializa el framework y lo pone en app

var enableCORS = function(req, res, next) {

 res.set("Access-Control-Allow-Origin", "*");

 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers", "Content-Type");

//  aqui habría que añadir también el JWT  (Jason Web Token)

 next();

}

// en postman tengo que tenerlo también indicado en la cabecera JSON (content-type)
app.use(express.json());  //le indico a express que le estoy enviando las cosas en JSON
app.use(enableCORS);

const authController = require('./controllers/AuthController');
const userController = require('./controllers/UserController');
const accountController = require('./controllers/AccountController');
const movController = require('./controllers/MovController');
const cmbdivController = require('./controllers/CmbDivController');
const utilController = require('./controllers/UtilController');

const port = process.env.PORT || 3000; //le asigna por defecto 3000 al no estar definida

app.listen(port) //pone en marcha el servidor

console.log("API escucchando en el puerto BIP BIP " + port);

// p1 y p2 son parámetros
app.post("/apitechu/prueba1/:p1/:p2",
  function(req, res){
    console.log("Parámetros");
    console.log(req.params);
    console.log("Query string");
    console.log(req.query);
    console.log("Headers");
    console.log(req.headers);
    console.log("Body");
    console.log(req.body);
    res.send({"msg":"Hola desde API Techu Prueba *****===================="}); //resultado en formato JSON
  }
)

app.post("/apitechu/login",
  authController.loginUser
)

app.post("/apitechu/logout/:id",
  authController.logoutUser
)

app.get("/apitechu/users/:id",
  userController.getUserById
)

app.get("/apitechu/cuentas/:id",
  accountController.getCuentasById
)

app.get("/apitechu/movimientos/:id",
  movController.getMovimientosById
)

app.post("/apitechu/movimientos",
  movController.crearMovimientos
)

app.post("/apitechu/user",
  userController.crearUsuario
)

app.get("/apitechu/cmbdiv/:id",
  cmbdivController.getCmbDivEur
)

app.post("/apitechu/cuenta/:id",
  accountController.crearCuenta
)

app.post("/apitechu/actsaldo",
  accountController.actSaldoCuenta
)

app.get("/apitechu/provincias",
  utilController.getProvincias
)

app.get("/apitechu/divisas",
  utilController.getDivisas
)

app.get("/apitechu/validausu",
  utilController.validaUsuario
)
