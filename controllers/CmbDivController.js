const requestJson = require('request-json');
const request = require('request');

const baseCMBDIVUrl = "https://api.cambio.today/v1/quotes/";
const cambioAPIKey = "apiKey=" + process.env.CAMBIO_API_KEY;

function getCmbDivEur (req, res){
  console.log("GET cambios de divisa");
//  console.log(req);
  console.log("Divisa " + req.params.id );

  var peticion = req.params.id + "/EUR/json?quantity=1" + "&key=" + cambioAPIKey;
//  console.log("peticion " + peticion);

var client = requestJson.createClient('https://api.cambio.today/v1/quotes/');

  client.get(peticion, function(err, resCmb, body) {
  if (err) {
    console.log(err);
    var response = {
        "msg" : "Error obteniendo cambios"
    }
    res.status(500);
  } else{
    var response = {
      cambio : body.result.value
    }
//  console.log(body.result.value);
  res.send(response);
  }
  });
}

module.exports.getCmbDivEur = getCmbDivEur;
