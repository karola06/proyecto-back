const validador = require("../validador");

// para conectar con mlab
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechucab12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getMovimientosById (req, res){
  console.log("GET /apitechu/movimientos/:id");

  var mensaje='';
  mensaje=validador.validaconsultamovimiento(req.params.id);
  console.log(mensaje.msg);
  console.log(mensaje.status);

  if (mensaje.status=="NOK"){
    console.log("caso1 " + mensaje)
    res.send(mensaje);
  } else {
    mensaje=validador.validacuenta(req.params.id);
    console.log(mensaje.msg);
    console.log(mensaje.status);

    if (mensaje.status=="NOK"){
      console.log("caso1 " + mensaje)
      res.send(mensaje);
    } else {


    var id = req.params.id;
    console.log("La id de la cuenta a obtener es: " + id);
    var query ='q={"idcuenta":'  + '"' + id +'"}'

    var httpClient = requestJson.createClient(baseMLABUrl);
    console.log("Client created");

    httpClient.get("movimientos?" + query + "&s={fechamov:1}&" + mLabAPIKey,
      function(err, resMlab, body){
        if (err) {
          var response = {
              "msg" : "Error obteniendo movimientos"
          }
          res.status(500);
        } else{
          if (body.length > 0){
            console.log(body);
            var response = body;
          } else{
            var response = {
              "status": "NOK",
              "msg": "No hay movimientos"
            };
          }
        }
        res.send(response);
      }
    );
  }
  }
}

function crearMovimientos(req, res){
  console.log("POST /apitechu/movimientos");
  var mensaje='';
  console.log(req.body.idcuenta);
  console.log(req.body.fechamov);
  console.log(req.body.concepto);
  console.log(req.body.tipomov);
  console.log(req.body.importe);

  mensaje=validador.validadatosmovimiento(req.body);
  console.log(mensaje.msg);
  console.log(mensaje.status);
  if (mensaje.status=="NOK"){
    console.log("caso1 " + mensaje)
    res.send(mensaje);
  } else {
    mensaje=validador.validafecha(req.body.fechamov);
    console.log(mensaje.msg);
    console.log(mensaje.status);
    if (mensaje.status=="NOK"){
      res.send(mensaje);
    } else {
      mensaje=validador.validatipomov(req.body.tipomov);
      console.log(mensaje.msg);
      console.log(mensaje.status);
      if (mensaje.status=="NOK"){
        res.send(mensaje);
      } else {
        mensaje=validador.validaimporte(req.body.importe);
        console.log(mensaje.msg);
        console.log(mensaje.status);
        if (mensaje.status=="NOK"){
          res.send(mensaje);
        } else {
          var newUser = {
            "idcuenta" : req.body.idcuenta,
            "fechamov" : req.body.fechamov,
            "concepto" : req.body.concepto,
            "tipomov" : req.body.tipomov,
            "importe" : req.body.importe
          }

          var httpClient = requestJson.createClient(baseMLABUrl);

          httpClient.post("movimientos?" + mLabAPIKey, newUser,
          function(err, resMlab, body){
            console.log("Movimiento creado");
            res.status(201);
            res.send({"msg":"movimiento registrado"});
          }
        )
        }
      }
    }
  }
}

module.exports.getMovimientosById = getMovimientosById;
module.exports.crearMovimientos = crearMovimientos;
