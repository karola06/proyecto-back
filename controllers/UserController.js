const crypt = require("../crypt");
const validador = require("../validador");

// para conectar con mlab
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechucab12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsers(req, res) {
  console.log("GET /apitechu/users");

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMlab, body){
      var response = !err ? body :{
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  );
}

function getUserById (req, res){
  console.log("GET /apitechu/users/:id");

  var id = req.params.id;
  console.log("La id del usuario a obtener es: " + id);
  var query ='q={"id":' + Number.parseInt(id) +'}';

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");


// para provocar el error 500 se puede poner un slash a user:
//  httpClient.get("/user?" + query + "&" + mLabAPIKey,
  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMlab, body){
    // body devuelve un array
/*    var response = !err ? body[0] :{
      "msg" : "Error obteniendo usuario"
    }*/
    if (err) {
      var response = {
            "msg" : "Error obteniendo usuario"
          }
      res.status(500);
      } else{
        if (body.length > 0){
          var response = body[0];
        } else{
          var response = {
                "msg" : "Usuario no encontrado"
              }
          res.status(404);
        }
    }
    res.send(response);
  }
);
}

function crearUsuario(req, res){
  var mensaje='';
  console.log("POST /apitechu/user");

  console.log(req.body.nombre);
  console.log(req.body.apellidos);
  console.log(req.body.email);
  console.log(req.body.direccion);
  console.log(req.body.provincia);
  console.log(req.body.codpostal);
  console.log(req.body.password);

  mensaje=validador.validadatosusuario(req.body);
  console.log(mensaje.msg);
  console.log(mensaje.status);
  if (mensaje.status=="NOK"){
    console.log("caso1 " + mensaje)
    res.send(mensaje);
  } else{
  mensaje=validador.validaemail(req.body.email);
  if (mensaje.status=="NOK"){
    console.log("caso2 " + mensaje)
    res.send(mensaje);
  } else {
  mensaje=validador.validapassword(req.body.password);
  if (mensaje.status=="NOK"){
    console.log("caso3 " + mensaje)
    res.send(mensaje);
  } else {
  mensaje=validador.validacodigopostal(req.body.codpostal);
  if (mensaje.status=="NOK"){
    console.log("caso4 " + mensaje)
    res.send(mensaje);
  } else {


  var query ='q={}&s={id:-1}&f={_id:0,id:1}&l=1';

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){
      // body devuelve un array */
      if (err) {
        var response = {
          "msg" : "Error obteniendo máximo usuario"
            }
        res.status(500);
        res.send(response);
        } else{
          var siguiente = Number.parseInt(body[0].id) + 1;
          console.log("siguiente " + siguiente)
          if (body.length > 0){
            var newUser = {
              "id" : siguiente,
              "nombre" : req.body.nombre,
              "apellidos" : req.body.apellidos,
              "email" : req.body.email,
              "direccion" : req.body.direccion,
              "provincia" : req.body.provincia,
              "codpostal": req.body.codpostal,
              "password" : crypt.hash(req.body.password)
            }
            httpClient.post("user?" + mLabAPIKey, newUser,
              function(err, resMlab, body2){
                console.log("Usuario creado");
                response = body2[0];
                res.status(201);
                res.send({"msg":"usuario creado"});
              }
            )
          } else{
            var response = {
                  "msg" : "Error dando de alta el usuario"
                }
            res.status(404);
            res.send(response);
          }
        }
      }
  );
}
}
}
}
}

module.exports.getUsers = getUsers;
module.exports.getUserById = getUserById;
module.exports.crearUsuario = crearUsuario;
